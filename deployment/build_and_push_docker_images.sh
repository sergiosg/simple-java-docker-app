#!/usr/bin/env bash


IMAGE_NAME=simplejavaapp
IMAGE_ID=sergiosg/$IMAGE_NAME
CI_COMMIT_SHA=$(git rev-parse HEAD)
CI_COMMIT_BRANCH=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')


# Extract version from the branch name, after stripping the prefix
VERSION=$(echo  "$(git rev-parse --abbrev-ref HEAD)" | sed -e 's,.*/\(.*\),\1,')
# Use Docker `latest` tag convention, if its from master branch, the image should be versioned as `latest`
[ "$VERSION" == "master" ] && VERSION=latest
echo IMAGE_ID=$IMAGE_ID
echo VERSION=$VERSION


# Login.
echo user "$DOCKERHUB_USER"
docker login -u "$DOCKERHUB_USER" -p "$DOCKERHUB_PASSWORD"

# Build image
docker build --tag $IMAGE_NAME  .
echo Image built and tagged as $IMAGE_NAME
# -------------------------
# Push
# -------------------------
# First push with the tag name derived from the branch name
docker tag $IMAGE_NAME $IMAGE_ID:$VERSION
echo Image taggeg as $IMAGE_NAME $IMAGE_ID:$VERSION
docker push $IMAGE_ID:$VERSION
echo Image pushed as $IMAGE_ID:$VERSION
# Now push with the SHA1 commit ID
docker tag $IMAGE_NAME $IMAGE_ID:"$CI_COMMIT_SHA"
echo Image taggeg as $IMAGE_NAME $IMAGE_ID:"$CI_COMMIT_SHA"
docker push $IMAGE_ID:"$CI_COMMIT_SHA"
echo Image pushed as $IMAGE_ID:"$CI_COMMIT_SHA"
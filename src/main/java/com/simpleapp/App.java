package com.simpleapp;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.net.URI;

public class App {

  private static final String BASE_URI = "http://0.0.0.0:8123/";

  protected static HttpServer startServer() {
    return GrizzlyHttpServerFactory.createHttpServer(
            URI.create(BASE_URI),
            new ResourceConfig().packages("com.simpleapp")
    );
  }

  public static void main(String[] args) {
    startServer();
    System.out.println(String.format("WADL available at %sapplication.wadl", BASE_URI));
    System.out.println(String.format("Endpoint available at %sstatus", BASE_URI));
  }
}


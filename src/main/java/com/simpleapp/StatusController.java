package com.simpleapp;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("status")
public class StatusController {

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String statusInformation() {
    System.out.println("Request received");
    return "{\"status\": \"idle\"}\n";
  }
}
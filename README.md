# Docker

**Step 1:** Build the Docker image 
```shell
$ docker build -t simpleapps/simplejavaapp:latest .
```

**Step 2:** Start a container for the Docker image.

```shell
$ docker run -p 8123:8123 simpleapps/simplejavaapp:latest
```

**Step 3:** Open another terminal and access the example API endpoint.

```shell
$ curl http://localhost:8123/status
{"status": "idle"}
```


# Kustomize



**Check yaml correctness**
```shell
kubectl kustomize deployment/kustomize/overlays/dev  
```

**Apply kustomize configuration**

```shell
kubectl apply -k deployment/kustomize/overlays/dev
```

**Delete kustomize configuration**
```shell
kubectl delete -k deployment/kustomize/overlays/dev
```

# Notes

You can also build, test, package, and run the Java application locally (without Docker)
if you have JDK 11+ and Maven installed.

```shell
# Build, test, package the application locally
$ mvn package

# Run the application locally
$ java -jar target/app.jar
```

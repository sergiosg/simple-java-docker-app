FROM openjdk:11.0.12-jre-slim
WORKDIR /root/
COPY ./target/app.jar .

EXPOSE 8123
ENTRYPOINT ["java", "-jar", "./app.jar"]
